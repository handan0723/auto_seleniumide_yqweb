import time
import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By


@pytest.fixture(scope='session', autouse=False)
def chrome_driver_localconfig():
    print("测试开始\n")
    user_data_dir = r'--user-data-dir=C:\Users\Administrator\AppData\Local\Google\Chrome\User Data'
    option = webdriver.ChromeOptions()
    option.add_argument(user_data_dir)
    option.add_argument("--remote-debugging-port=9222")
    driver = webdriver.Chrome(chrome_options=option)
    # driver = webdriver.Chrome(options=option)
    yield driver
    print("\n测试完毕，关闭浏览器\n")
    driver.quit()

