# Generated by Selenium IDE
import pytest
import time
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


class TestPersonalPage:

    def test_personalPage(self, chrome_driver_localconfig):
        chrome_driver_localconfig.get("https://youqian.360.cn/index.html")
        chrome_driver_localconfig.maximize_window()
        time.sleep(1)

        chrome_driver_localconfig.find_element(By.LINK_TEXT, "首页").click()
        time.sleep(1)
        chrome_driver_localconfig.find_element(By.LINK_TEXT, "推广产品").click()
        time.sleep(1)
        chrome_driver_localconfig.find_element(By.LINK_TEXT, "活动").click()
        time.sleep(1)
        chrome_driver_localconfig.find_element(By.LINK_TEXT, "安全盾").click()
        time.sleep(1)
        chrome_driver_localconfig.find_element(By.LINK_TEXT, "个人中心").click()
        time.sleep(1)
        chrome_driver_localconfig.find_element(By.LINK_TEXT, "帮助中心").click()
        time.sleep(1)
        chrome_driver_localconfig.find_element(By.LINK_TEXT, "公告").click()
        time.sleep(1)
