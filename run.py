import os
import time
import pytest


if __name__ == "__main__":
    # pytest.main(['-vs'])
    pytest.main(['-vs', './testcases/test_login.py'])
    pytest.main(['-vs', './testcases/test_index.py'])
    pytest.main(['-vs', './testcases/test_changeTab.py'])
    pytest.main(['-vs', './testcases/test_personalPage.py'])
    pytest.main(['-vs', './testcases/test_logout.py'])
    # jenkins使用
    # pytest.main(['-vs', '--alluredir=./report/allure-results', '--clean-alluredir'])
    time.sleep(2)
    # os.system("allure generate ./report/allure-results -o ./report/allure --clean")


